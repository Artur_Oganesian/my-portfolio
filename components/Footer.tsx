import React from "react";
import Link from "next/link";
import { useTranslations } from "next-intl";
import MagicButton from "@/components/ui/MagicButton";
import { FaLocationArrow } from "react-icons/fa6";
import { socialMedia } from "@/data";


const Footer = () => {
  const t = useTranslations("footer");
  const currentYear = new Date().getFullYear();
  return (
    <footer className="w-full mb-[100px] md:mb-5 pb-10" id="contacts">
      <div className="flex flex-col items-center">
        <h1 className="heading lg:max-w-[45vw]">{t("title")}</h1>
        <p className="text-purple md:mt-10 my-5 text-center text-xl">
          {t("desc")}
        </p>
        <a href="mailto:arturoganesianofficial@gmail.com">
          <MagicButton
            title={t("button")}
            icon={<FaLocationArrow />}
            position="right"
          />
        </a>
      </div>
      <div className="flex mt-16 md:flex-row  gap-5 flex-col justify-between items-center">
        <p className="md:text-base text-sm md:font-normal font-light">
          {currentYear} Artur Oganesian
        </p>
        <div className="flex items-center md:gap-3 gap-6">
          {socialMedia.map(({ link, id, img }) => (
            <Link
              href={link}
              target="_blank"
              key={id}
              className="w-10 h-10 cursor-pointer flex justify-center items-center backdrop-filter backdrop-blur-lg saturate-180 bg-opacity-75
bg-black-200 rounded-lg border border-black-300"
            >
              <img src={img} alt={"profile"} width={20} height={20} />
            </Link>
          ))}
        </div>
      </div>
    </footer>
  );
};

export default Footer;
