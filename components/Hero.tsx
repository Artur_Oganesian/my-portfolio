"use client";

import React from "react";
import { useTranslations } from "next-intl";
import { Spotlight } from "@/components/ui/Spotlight";
import { TextGenerateEffect } from "@/components/ui/TextGenerateEffect";
import MagicButton from "@/components/ui/MagicButton";
import LanguageSwitcher from "@/components/LanguageSwitcher";
import { FaLocationArrow } from "react-icons/fa6";


const Hero = () => {
  const t = useTranslations("hero");
  return (
    <div className="pb-20 lg:pt-36 pt-[5rem]">
      <div>
        <Spotlight
          className="-top-40 -left-10 md:-left-32 md:-top-20 h-screen"
          fill="white"
        />
        <Spotlight
          className="top-10 left-full h--[80vh] w-[50vw]"
          fill="purple"
        />
        <Spotlight className="-top-28 left-80 h-[80vh] w-[50wh]" fill="blue" />
      </div>
      <div className="h-screen w-full dark:bg-black-100 bg-white  dark:bg-grid-white/[0.05] bg-grid-black/[0.2] flex items-center justify-center absolute top-0 left-0">
        <div className="absolute pointer-events-none inset-0 flex items-center justify-center dark:bg-black-100 bg-white [mask-image:radial-gradient(ellipse_at_center,transparent_20%,black)]" />
      </div>

      <div className="flex justify-center relative m-y-20 z-10">
        <div className="max-w-[89vw] md:max-w-2xl lg:max-w-[60vw] flex flex-col items-center justify-center">
         
          <TextGenerateEffect
            className="text-center text-[40px] md:text-5xl lg:text-6xl"
            words={t("title")}
          />
          <p className="text-center md:tracking-wider mb-4 text-sm md:text-lg lg:text-xl ">
            {t("aboutme")}
          </p>
          <div className="flex flex-col items-center">
          <a href="#about">
            <MagicButton
              title={t("show_work")}
              icon={<FaLocationArrow />}
              position="right"
            />
          </a>
          <a href="https://gitlab.com/Artur_Oganesian/my-portfolio" target="_blank">
            <MagicButton
              title={t("see_code")}
              icon={<FaLocationArrow />}
              position="right"
            />
          </a>
          </div>
         
          <LanguageSwitcher />
        </div>
      </div>
    </div>
  );
};

export default Hero;
