import React from "react";
import { useRouter, usePathname } from "next/navigation";
import { useTranslations } from "next-intl";
import { Dropdown, Button } from "antd";

const LanguageSwitcher = () => {
  const pathname = usePathname();
  const router = useRouter();

  const t = useTranslations("simple");
  const items = [
    {
      key: "am",
      label: (
        <Button
          onClick={() => router.push(pathname.replace(/^\/[a-z]{2}/, `/am`))}
          className="bg-black text-purple"
        >
          Հայերեն
        </Button>
      ),
    },
    {
      key: "ru",
      label: (
        <Button
          onClick={() => router.push(pathname.replace(/^\/[a-z]{2}/, `/ru`))}
          className="bg-black text-purple"
        >
          Русский
        </Button>
      ),
    },
    {
      key: "en",
      label: (
        <Button
          onClick={() => router.push(pathname.replace(/^\/[a-z]{2}/, `/en`))}
          className="bg-black text-purple"
        >
          English
        </Button>
      ),
    },
  ];

  return (
    <div className="mt-5">
      <Dropdown
        menu={{
          items,
        }}
        placement="bottom"
        arrow
        overlayClassName="!bg-black-100 rounded-full"
      >
        <button
          onClick={(e) => e.preventDefault()}
          className="inline-flex h-10 animate-shimmer text-sm items-center justify-center rounded-md border border-slate-800 bg-[linear-gradient(110deg,#000103,45%,#1e2631,55%,#000103)] bg-[length:200%_100%] px-6 font-medium text-slate-400 transition-colors focus:outline-none focus:ring-2 focus:ring-slate-400 focus:ring-offset-2 focus:ring-offset-slate-50"
        >
          {t("change_language")}
        </button>
      </Dropdown>
    </div>
  );
};

export default LanguageSwitcher;
