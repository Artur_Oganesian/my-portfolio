export const navItems = [
  { name: "about", link: "#about" },
  { name: "projects", link: "#projects" },
  { name: "contact", link: "#contact" },
];

export const gridItems = [
  {
    id: 1,
    title: "client_communication",
    description: "",
    className: "lg:col-span-3 md:col-span-6 md:row-span-4 lg:min-h-[60vh]",
    imgClassName: "w-full h-full",
    titleClassName: "justify-end",
    img: "/b1.svg",
    spareImg: "",
  },
  {
    id: 2,
    title: "time_zone",
    description: "",
    className: "lg:col-span-2 md:col-span-3 md:row-span-2",
    imgClassName: "",
    titleClassName: "justify-start",
    img: "",
    spareImg: "",
  },
  {
    id: 3,
    title: "title",
    description: "stack_slogan",
    className: "lg:col-span-2 md:col-span-3 md:row-span-2",
    imgClassName: "",
    titleClassName: "justify-center",
    img: "",
    spareImg: "",
  },
  {
    id: 4,
    title: "human_skills",
    description: "",
    className: "lg:col-span-2 md:col-span-3 md:row-span-1",
    imgClassName: "",
    titleClassName: "justify-start",
    img: "/grid.svg",
    spareImg: "/b4.svg",
  },

  {
    id: 5,
    title: "learning_node",
    description: "new_technologies",
    className: "md:col-span-3 md:row-span-2",
    imgClassName:
      "absolute right-0  md:right-[25%] lg:right-0 bottom-8 sm:bottom-[20%] md:w-[200px] lg:w-80 xl:w-96  lg:h-[70%]  w-[100px] sm:h-[100px] md:h-[200px]",
    titleClassName: "justify-center md:justify-start lg:justify-center",
    img: "/nodejs.png",
  },
  {
    id: 6,
    title: "start_with_me",
    description: "",
    className: "lg:col-span-2 md:col-span-3 md:row-span-1",
    imgClassName: "",
    titleClassName: "justify-center md:max-w-full max-w-60 text-center",
    img: "",
    spareImg: "",
  },
];

export const projects = [
  {
    id: 1,
    title: "e_commerce",
    position: "developer",
    des: "e_commerce_desc",
    img: "/sellersam3.png",
    iconLists: [
      "/next.svg",
      "/tail.svg",
      "/ts.svg",
      "/nodejs.png",
      "redux.png",
      "/ant-design-seeklogo.svg",
    ],
    link: "https://www.sellers.am/am",
  },
  {
    id: 2,
    title: "weather_app",
    position: "developer",
    des: "weather_app_desc",
    img: "/weatherapp.png",
    iconLists: [
      "/re.svg",
      "/sass-seeklogo.svg",
      "/js.svg",
      "/redux.png",
      "/ant-design-seeklogo.svg",
    ],
    link: "https://weather-app-five-alpha-20.vercel.app/",
  },
  // {
  //   id: 3,
  //   title: "star_wars",
  //   position: "developer",
  //   des: "star_wars_desc",
  //   img: "/starwars.png",
  //   iconLists: [
  //     "/re.svg",
  //     "/sass-seeklogo.svg",
  //     "/js.svg",
  //     "/ant-design-seeklogo.svg",
  //   ],
  //   link: "https://star-wars-zsxw.vercel.app/",
  // },
  {
    id: 3,
    title: "car_hub",
    position: "owner",
    des: "car_hub_desc",
    img: "/carhubbig.png",
    iconLists: [
      "/next.svg",
      "/tail.svg",
      "/ts.svg",
      "/headless-ui-seeklogo.svg",
    ],
    link: "https://cars-e-commerce-app.vercel.app/",
  },
  {
    id: 4,
    title: "other_projects",
    position: "",
    des: "other_projects_desc",
    img: "/checkmyrepos.webp",
    iconLists: ["/github.svg", "/gitlabrepo.svg"],
    link: "https://gitlab.com/Artur_Oganesian",
  },
];

export const testimonials = [
  {
    quote:
      "Collaborating with Adrian was an absolute pleasure. His professionalism, promptness, and dedication to delivering exceptional results were evident throughout our project. Adrian's enthusiasm for every facet of development truly stands out. If you're seeking to elevate your website and elevate your brand, Adrian is the ideal partner.",
    name: "Michael Johnson",
    title: "Director of AlphaStream Technologies",
  },
  {
    quote:
      "Collaborating with Adrian was an absolute pleasure. His professionalism, promptness, and dedication to delivering exceptional results were evident throughout our project. Adrian's enthusiasm for every facet of development truly stands out. If you're seeking to elevate your website and elevate your brand, Adrian is the ideal partner.",
    name: "Michael Johnson",
    title: "Director of AlphaStream Technologies",
  },
  {
    quote:
      "Collaborating with Adrian was an absolute pleasure. His professionalism, promptness, and dedication to delivering exceptional results were evident throughout our project. Adrian's enthusiasm for every facet of development truly stands out. If you're seeking to elevate your website and elevate your brand, Adrian is the ideal partner.",
    name: "Michael Johnson",
    title: "Director of AlphaStream Technologies",
  },
  {
    quote:
      "Collaborating with Adrian was an absolute pleasure. His professionalism, promptness, and dedication to delivering exceptional results were evident throughout our project. Adrian's enthusiasm for every facet of development truly stands out. If you're seeking to elevate your website and elevate your brand, Adrian is the ideal partner.",
    name: "Michael Johnson",
    title: "Director of AlphaStream Technologies",
  },
  {
    quote:
      "Collaborating with Adrian was an absolute pleasure. His professionalism, promptness, and dedication to delivering exceptional results were evident throughout our project. Adrian's enthusiasm for every facet of development truly stands out. If you're seeking to elevate your website and elevate your brand, Adrian is the ideal partner.",
    name: "Michael Johnson",
    title: "Director of AlphaStream Technologies",
  },
];

export const companies = [
  {
    id: 1,
    name: "cloudinary",
    img: "/cloud.svg",
    nameImg: "/cloudName.svg",
  },
  {
    id: 2,
    name: "appwrite",
    img: "/app.svg",
    nameImg: "/appName.svg",
  },
  {
    id: 3,
    name: "HOSTINGER",
    img: "/host.svg",
    nameImg: "/hostName.svg",
  },
  {
    id: 4,
    name: "stream",
    img: "/s.svg",
    nameImg: "/streamName.svg",
  },
  {
    id: 5,
    name: "docker.",
    img: "/dock.svg",
    nameImg: "/dockerName.svg",
  },
];

export const workExperience = [
  {
    id: 1,
    title: "intern",
    desc: "intern_desc",
    className: "md:col-span-2",
    thumbnail: "/exp1.svg",
  },
  {
    id: 4,
    title: "junior",
    desc: "junior_desc",
    className: "md:col-span-2",
    thumbnail: "/exp4.svg",
  },
];

export const socialMedia = [
  {
    id: 1,
    img: "/git.svg",
    link: "https://github.com/ArturOganesian",
  },
  {
    id: 2,
    img: "/gitlab.svg",
    link: "https://gitlab.com/Artur_Oganesian",
  },
  {
    id: 3,
    img: "/link.svg",
    link: "https://www.linkedin.com/in/artur-oganesian/",
  },
];
